﻿using System.Data.Entity;
using ExtentionEntities.Entities;

namespace ExtentionEntities
{
    public class DataContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Process> Processes { get; set; }
        public DbSet<CurrentUserInformation> CurrentUserInformations { get; set; }

    }
}
