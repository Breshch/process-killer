﻿namespace ExtentionEntities.Entities
{
    public class Process
    {
        public int Id { get; set; }
        public string ProcessName { get; set; }
        public int UserId { get; set; }
    }
}
