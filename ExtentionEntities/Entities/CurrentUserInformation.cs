﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace ExtentionEntities.Entities
{
    public class CurrentUserInformation
    {
        [Key]
        [ForeignKey("User")]
        public int UserId { get; set; }
        public int SpentMinutes { get; set; }
        public DateTime? LastProcessStartedDate { get; set; }

        [XmlIgnore]
        public User User { get; set; }
    }
}
