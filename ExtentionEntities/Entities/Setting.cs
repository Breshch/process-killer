﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace ExtentionEntities.Entities
{
    public class Setting
    {
        [Key]
        [ForeignKey("User")]
        public int UserId { get; set; }
        public int MaxSpentMinutes { get; set; }

        [XmlIgnore]
        public User User { get; set; }
    }
}
