﻿using System.Collections.Generic;

namespace ExtentionEntities.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Setting Setting { get; set; }
        public CurrentUserInformation CurrentUserInformation { get; set; }

        public List<Process> Processes { get; set; }
    }
}
