﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace UsersLimit.Models
{
    public class UsersViewModel
    {
        public string SelectedUserId { get; set; }
        public SelectListItem[] Users { get; set; }
    }
}