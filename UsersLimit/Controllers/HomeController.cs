﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using ExtentionEntities;
using ExtentionEntities.Entities;
using Microsoft.ApplicationInsights.Extensibility.Implementation;
using Newtonsoft.Json;
using UsersLimit.Models;

namespace UsersLimit.Controllers
{
    public class HomeController : Controller
    {
        private readonly DataContext _dataContext;

        public HomeController()
        {
            _dataContext = new DataContext();
        }

        public ActionResult ProfileStatus()
        {
             return View();
        }

        public ActionResult Profiles()
        {
            return View();
        }

        public ActionResult Processes()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetUsers()
        {
            var users = _dataContext.Users.Include(x => x.Setting).Include(x => x.CurrentUserInformation)
                .Include(x => x.Processes).OrderBy(x => x.Id).ToArray();

            var jsonUsers = JsonConvert.SerializeObject(users, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

            return Json(jsonUsers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ChangeMaxSpentMinutes(int userId, int minutes)
        {
            var user = _dataContext.Users.Include(x => x.Setting).First(x => x.Id == userId);
            user.Setting.MaxSpentMinutes += minutes;
            _dataContext.SaveChanges();

            return Json(user.Setting.MaxSpentMinutes);
        }

        [HttpGet]
        public JsonResult GetSpentMinutes(int userId)
        {
            var minutes = _dataContext.Users.Include(x => x.CurrentUserInformation)
                .First(x => x.Id == userId).CurrentUserInformation.SpentMinutes;
            return Json(minutes, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddUser(string userName)
        {
            var user = new User
            {
                CurrentUserInformation = new CurrentUserInformation
                {
                    SpentMinutes = 0,
                },
                Name = userName,
                Setting = new Setting
                {
                    MaxSpentMinutes = 120
                }
            };
            _dataContext.Users.Add(user);
            _dataContext.SaveChanges();

            var jsonUser = JsonConvert.SerializeObject(user, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

            return Json(jsonUser);
        }

        [HttpPost]
        public JsonResult DeleteUser(int userId)
        {
            var user = _dataContext.Users.Find(userId);
            _dataContext.Users.Remove(user);
            _dataContext.SaveChanges();

            return Json(true);
        }


        [HttpPost]
        public JsonResult DeleteProcess(int processId)
        {
            var process = _dataContext.Processes.Find(processId);
            _dataContext.Processes.Remove(process);
            _dataContext.SaveChanges();

            var user = _dataContext.Users.Include(x => x.Processes).First(x => x.Id == process.UserId);

            var jsonUser = JsonConvert.SerializeObject(user, Formatting.Indented,
                new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

            return Json(jsonUser);
        }

        [HttpPost]
        public JsonResult AddProcess(int userId, string processName)
        {
            var process = new Process
            {
                ProcessName = processName,
                UserId = userId
            };
            _dataContext.Processes.Add(process);
            _dataContext.SaveChanges();

            return Json(process);
        }
    }
}