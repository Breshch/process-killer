﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Timers;
using System.Xml.Serialization;
using ExtentionEntities;
using ExtentionEntities.Entities;
using Process = System.Diagnostics.Process;

namespace Extention_64
{
    public partial class ProcessMainLook_32 : ServiceBase
    {
        private readonly string _pathToProgramAssemblyLocation;

       
        public ProcessMainLook_32()
        {
            InitializeComponent();
            _pathToProgramAssemblyLocation = this.GetType().Assembly.Location;
            _pathToProgramAssemblyLocation = Path.GetDirectoryName(_pathToProgramAssemblyLocation);
        }

        protected override void OnStart(string[] args)
        { 
            Timer timer = new Timer();
            timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
            timer.Interval = 60000; //number in milisecinds  
            timer.Enabled = true;
        }

        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            ProcessLookUp();
        }

        private void ProcessLookUp()
        {
            string userName = ConfigurationManager.AppSettings["UserName"];

            var user = GetUser(userName);

            bool hasLookUpProcess = false;
            foreach (var lookingUpProcess in user.Processes)
            {
                foreach (var process in Process.GetProcessesByName(lookingUpProcess.ProcessName))
                {
                    hasLookUpProcess = true;
                    if (user.CurrentUserInformation.LastProcessStartedDate == null)
                    {
                        user.CurrentUserInformation.LastProcessStartedDate = DateTime.Now;
                    }
                }
            }

            if (hasLookUpProcess)
            {
                user.CurrentUserInformation.SpentMinutes += 1;
            }

            if (user.CurrentUserInformation.SpentMinutes >= user.Setting.MaxSpentMinutes)
            {
                foreach (var lookingUpProcess in user.Processes)
                {
                    foreach (var process in Process.GetProcessesByName(lookingUpProcess.ProcessName))
                    {
                        process.Kill();
                    }
                }
            }

            if (user.CurrentUserInformation.LastProcessStartedDate.HasValue && user.CurrentUserInformation.LastProcessStartedDate.Value.Date != DateTime.Now.Date)
            {
                user.CurrentUserInformation.SpentMinutes = 0;
                user.CurrentUserInformation.LastProcessStartedDate = null;
            }

            SaveLocalUser(user);
            SaveCurrentUserInformation(user);
        }

        private User GetUser(string name)
        {
            try
            {
                using (var db = new DataContext())
                {
                    var user = db.Users.Include(x => x.Setting).Include(x => x.Processes).Include(x => x.CurrentUserInformation).First(x => x.Name == name);
                    
                    return user;
                }
            }
            catch (Exception)
            {
                return GetLocalUser();
            }
        }


        private User GetLocalUser()
        {
            if (File.Exists(Path.Combine(_pathToProgramAssemblyLocation, "extentions_64.dll")))
            {
                var xmlSerializer = new XmlSerializer(typeof(User));
                using (var reader = new StreamReader(Path.Combine(_pathToProgramAssemblyLocation, "extentions_64.dll")))
                {
                    return (User)xmlSerializer.Deserialize(reader);
                }
            }
            else
            {
                return new User
                {
                    Setting = new Setting
                    {
                        MaxSpentMinutes = 100
                    },
                    Processes = new List<ExtentionEntities.Entities.Process>()
                };
            }
        }


        private void SaveLocalUser(User user)
        {
            string localPath = Path.Combine(_pathToProgramAssemblyLocation, "extentions_64.dll");

            using (StreamWriter writer = new StreamWriter(localPath, false))
            {
                var xmlSerializer = new XmlSerializer(typeof(User));
                xmlSerializer.Serialize(writer, user);
            }
        }

        private void SaveCurrentUserInformation(User user)
        {
            using (var dc = new DataContext())
            {
                var currentUserInformation = dc.CurrentUserInformations.FirstOrDefault(x => x.UserId == user.Id);
                if (currentUserInformation != null)
                {
                    currentUserInformation.SpentMinutes = user.CurrentUserInformation.SpentMinutes;
                    currentUserInformation.LastProcessStartedDate = user.CurrentUserInformation.LastProcessStartedDate;
                    dc.SaveChanges();
                }
            }
        }
    }
}
