﻿using System;

namespace Extention_64
{
    public class Locals
    {
        public DateTime? StartDateTime { get; set; }
        public int SpentMinutes { get; set; }
        public DateTime CurrentDate { get; set; }
    }
}
