﻿using System.ServiceProcess;

namespace Extention_64
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ProcessMainLook_32()
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
